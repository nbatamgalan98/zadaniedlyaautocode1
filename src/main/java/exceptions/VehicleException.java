package exceptions;

public abstract class VehicleException extends Exception {

    public VehicleException(String message) {
        super(message);
    }
}
