package exceptions;

public class NotLoadedException extends VehicleException {
    public NotLoadedException() {
        super("nothing loaded - unloading aborted");
    }
}