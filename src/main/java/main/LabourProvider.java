package main;

public interface LabourProvider {
    int provideDayLabourers(int numRequired);
}