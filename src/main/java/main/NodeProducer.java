package main;

public interface NodeProducer {
    Material getProducedMaterial();
    double getProductionPerDay();
    double getMaxStorage();
    double getStoredAmount();
    void produce();
}