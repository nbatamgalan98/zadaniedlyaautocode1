package impl;

import main.Node;
import main.Road;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractNodeImpl implements Node {

    private Map<Node, Road> neighbours;

    public AbstractNodeImpl() {
        this.neighbours = new HashMap<>();
    }

    @Override
    public void addNeighbour(Node neighbour, Road road) {
        neighbours.put(neighbour, road);
    }

    @Override
    public Set<Node> getNeighbours() {
        return neighbours.keySet();
    }

    @Override
    public Road getRoadToNeighbour(Node neighbour) {
        return neighbours.get(neighbour);
    }
}