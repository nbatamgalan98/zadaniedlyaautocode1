package impl;
import main.Node;
import main.NodeToll;

public class TollNodeImpl extends AbstractNodeImpl implements Node, NodeToll {

    private double tollAmount;

    public TollNodeImpl(double tollAmount) {
        super();

        this.tollAmount = tollAmount;
    }


    @Override
    public double getTollAmount() {
        return tollAmount;
    }
}
