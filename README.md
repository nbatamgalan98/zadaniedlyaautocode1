Заменить скорость по себестоимости
---
### У нас будут следующие объекты :
* Узел: подумайте об этом как о точках интереса в транспортной сети (производитель, пункт взимания платы, строительная площадка).

* Дорога: в нашей упрощенной модели дорога — это соединение, которое можно использовать для перемещения между двумя узлами.

* Транспортное средство: транспортное средство перемещается между узлами и может забирать материалы на складе и сбрасывать их на строительной площадке.

* Материал: материалы необходимы для завершения сборки и должны быть доставлены от производителей на строительные площадки.



---
Узел должен иметь:
```
Иметь имя

Иметь тип:

        - режиссер
        
        - потребитель
        
        - потери
        
Список транспортных средств

Карта соседей подключенных узлов со значениями,сопоставленными 
с дорогой, чтобы получить подробную информацию о расстоянии
```

Дорога должна иметь:
```
Расстояние (в км)

Ограничение скорости (в км/ч)

2 узла
```


Материал может быть реализован как перечисление, которое должно иметь:
```
Вес в метрических тоннах

Плотность в кубических метрах на метрическую тонну (умножьте это на вес, чтобы определить объем)
Удобное имя и тип:
        
        - кирпичи (плотность: 1,89)
        
        - бетон (плотность: 2,4)
        
        - песок (плотность: 1,6)
       
        - вода (плотность: 1,0)
```

Транспортное средство перевозит материал — мы сделаем упрощающее предположение, что транспортное средство может перевозить только один тип материала за раз.
- Транспортное средство должно иметь:
```
Удобное имя

WeightLimit — это максимальный вес, который можно перевозить в метрических тоннах.

volumeLimit — максимальный объем, который можно перевезти в кубических метрах

typeLimit — разрешенный список типов Материалов, которые можно переносить

cargoType - это текущий материал (или нуль)

CargoWeight – текущий вес перевозимого материала в метрических тоннах (или ноль).

maxSpeed — максимальная скорость в км/ч.

maxSpeedLoaded — максимальная скорость при полной загрузке в км/ч.

currentSpeed (расчетная): текущая скорость с учетом нагрузки
```